const express = require("express");

const app = express();
app.use(express.json());

const port = 8000;

const {userRouter} = require("./app/routes/userRoute");

app.use("/",(req,res,next)=>{
    console.log("Date: "+ new Date());
    next();
});


app.get("/",(req, res)=>{
   return res.status(200).json({
    message: "test"
   })
});

app.use("/", userRouter);

app.listen(port,()=>{
    console.log("Connect to port: "+port);
})