const express = require("express");

const userRouter = express.Router();

const {getUser} = require("../middlewares/userMiddleware");

var data = require("../../data");

userRouter.get("/users", getUser, (req, res)=>{
    let query = req.query;
    let age = query.age;
    if(age != null){
        let newData = data.user.filter((user)=>{
            return user.age > age;
        })

        return res.status(200).json({
            newData
        })
    }else{
        return res.status(200).json({
            data
        })
    }
})

userRouter.get("/users/:id", getUser, (req, res)=>{
    let id = req.params.id;
    if(!Number.isInteger(parseInt(id))){
        return res.status(400).json({
            message: `Bad request: Id(${id}) khong dung`
        })
    }

    let newData = data.user.filter((user)=>{
        return user.id == id;
    })

    return res.status(200).json({
        newData
    })
});

module.exports = {userRouter};